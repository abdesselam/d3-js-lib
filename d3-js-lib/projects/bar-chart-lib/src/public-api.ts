/*
 * Public API Surface of bar-chart-lib
 */

export * from './lib/bar-chart-lib.component';
export * from './lib/bar-chart-lib.module';
