import { NgModule } from '@angular/core';
import { BarChartLibComponent } from './bar-chart-lib.component';



@NgModule({
  declarations: [
    BarChartLibComponent
  ],
  imports: [
  ],
  exports: [
    BarChartLibComponent
  ]
})
export class BarChartLibModule { }
