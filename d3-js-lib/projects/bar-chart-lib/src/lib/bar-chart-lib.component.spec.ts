import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BarChartLibComponent } from './bar-chart-lib.component';

describe('BarChartLibComponent', () => {
  let component: BarChartLibComponent;
  let fixture: ComponentFixture<BarChartLibComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BarChartLibComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BarChartLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
